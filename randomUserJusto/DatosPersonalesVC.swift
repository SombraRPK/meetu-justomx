//
//  DatosPersonalesVC.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 03/12/20.
//

import UIKit

class DatosPersonalesVC: UIViewController {

    var datosPersonales: datosPersonalesUsuario?
    
    @IBOutlet weak var imagenUsuario: UIImageView!
    @IBOutlet weak var etiquetaNombre: UILabel!
    @IBOutlet weak var etiquetaEdad: UILabel!
    @IBOutlet weak var campoTextoDirección: UITextView!
    @IBOutlet weak var campoTextoContacto: UITextView!
    @IBOutlet weak var campoTextoOtrosDatos: UITextView!
    @IBOutlet weak var botónMapa: UIButton!
    
    @IBAction func botónMapaAcción(_ sender: Any) {
        let pantallaMapa = storyboard?.instantiateViewController(withIdentifier: "vistaMapa") as! MapaVC
        pantallaMapa.nombre = (datosPersonales?.results![0].name?.first!)!
        pantallaMapa.coordenadasRecibidas = coordenadasFlotantes(
            latitud: (
                ((datosPersonales?.results![0].location?.coordinates?.latitude!)! as NSString).floatValue
        ), longitud: (
            ((datosPersonales?.results![0].location?.coordinates?.longitude!)! as NSString).floatValue
        ))
        navigationController?.pushViewController(pantallaMapa, animated: true)
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        prepararUI()
        vaciarDatosEnPantalla()
    }
    
    func prepararUI(){
        //Imagen perfil
        imagenUsuario.layer.cornerRadius = imagenUsuario.frame.height/2
        
        //Botones
        botónMapa.setImage(UIImage(named: "Mapa")?.withRenderingMode(.alwaysOriginal), for: .normal)
        botónMapa.imageView?.contentMode = .scaleToFill
    }
    
    func vaciarDatosEnPantalla(){
        
        //Imagen perfil
        imagenUsuario.cargarDesdeWeb(url: URL(string: (datosPersonales?.results![0].picture?.medium)!)!)
        imagenUsuario.layer.borderWidth = 4
        if (datosPersonales?.results![0].gender == "male") {
            imagenUsuario.layer.borderColor = UIColor (red: 0.05, green: 0.42, blue: 0.95, alpha: 0.4).cgColor
        } else {
            imagenUsuario.layer.borderColor = UIColor (red: 0.7, green: 0.1, blue: 0.5, alpha: 0.4).cgColor
        }
        
        //Nombre
        etiquetaNombre.text = datosPersonales?.results![0].name?.first!
        
        //Edad
        etiquetaEdad.text = "\(datosPersonales!.results![0].dob!.age!) años"
        
        //Dirección
        let dirección = datosPersonales!.results![0].location
        campoTextoDirección.text = "Calle \(dirección!.street!.name!), no. \(dirección!.street!.number!), \(dirección!.city!), \(dirección!.country!), C.P. \(dirección!.postcode!)"
        
        //Contacto
        let contacto = datosPersonales!.results![0]
        campoTextoContacto.text = "Email: \(contacto.email!) \nTeléfono: \(contacto.phone!)\nCelular: \(contacto.cell!)"
        
        //Otros datos
        let informaciónGeneral = datosPersonales!.results![0]
        let cumpleaños = darFormatoAFecha(fecha: informaciónGeneral.dob!.date!)
        let fechaAlta = darFormatoAFecha(fecha: informaciónGeneral.registered!.date!)
        campoTextoOtrosDatos.text = "Cumpleaños: \(cumpleaños)\nMiembro desde: \(fechaAlta)"
    }
}
