//
//  ResultadosVC.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 02/12/20.
//

import UIKit

class ResultadosVC: UIViewController {
    
    private var datosUsuario: datosPersonalesUsuario?
    private var animaciónCarga = AnimaciónCarga()
    
    @IBOutlet weak var imagenPerfil: UIImageView!
    @IBOutlet weak var imagenGénero: UIImageView!
    @IBOutlet weak var etiquetaTítulo: UILabel!
    @IBOutlet weak var etiquetaNombre: UILabel!
    @IBOutlet weak var etiquetaApellido: UILabel!
    @IBOutlet weak var botónSiguientePerfil: botónRedondo!
    @IBOutlet weak var botónMásInfo: botónRedondo!
    @IBOutlet weak var botónInterés: botónRedondo!
    
    @IBAction func botónSiguientePerfilAcción(_ sender: Any) {
        realizarPeticiónDePerfil()
    }
    
    @IBAction func botónMásInfoAcción(_ sender: Any) {
        let pantallaDatosPersonales = storyboard?.instantiateViewController(withIdentifier: "pantallaDatosPersonales") as! DatosPersonalesVC
        pantallaDatosPersonales.datosPersonales = datosUsuario
        if datosUsuario != nil {
            navigationController?.pushViewController(pantallaDatosPersonales, animated: true)
        } else {
            // Mostrar alerta de datos no cargados
        }
    }
    
    @IBAction func botónInterésAcción(_ sender: Any) {
        AlertaInteresante.instancia.mostrarAlerta()
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        realizarPeticiónDePerfil()
        prepararUI()
    }
    
    private func prepararUI(){
        
        //title = "Nuevo perfil encontrado!"
        
        // Foto de perfil
        imagenPerfil.layer.cornerRadius = imagenPerfil.frame.height/2
        imagenPerfil.contentMode = .scaleAspectFit
        imagenPerfil.layer.borderColor = UIColor (red: 0.3, green: 0.3, blue: 0.3, alpha: 0.4).cgColor
        imagenPerfil.layer.borderWidth = 10
    
        // Botón siguiente perfil
        botónSiguientePerfil.setImage(UIImage(named: "Siguiente")?.withRenderingMode(.alwaysOriginal), for: .normal)
        botónSiguientePerfil.imageView?.contentMode = .scaleToFill
        
        // Botón más información
        botónMásInfo.setImage(UIImage(named: "Información")?.withRenderingMode(.alwaysOriginal), for: .normal)
        botónMásInfo.imageView?.contentMode = .scaleToFill
        
        // Botón me interesa
        botónInterés.setImage(UIImage(named: "Estrella")?.withRenderingMode(.alwaysOriginal), for: .normal)
        botónInterés.imageView?.contentMode = .scaleToFill
    }
    
    private func validarRespuestaPetición(para respuesta: Data) -> datosPersonalesUsuario? {  //Verificamos que tengamos en la respuesta una estructura tipo JSON
        let decodificadorJSON = JSONDecoder()
        do {
            datosUsuario = try decodificadorJSON.decode (datosPersonalesUsuario.self, from: respuesta)
            print (datosUsuario!)
        } catch {
            print ("Error al decodificar JSON: \(error.localizedDescription)")
        }
        return datosUsuario
    }
    
    private func vaciarDatosEnPantalla(para usuario: datosPersonalesUsuario) {
        
        // Foto
        imagenPerfil.cargarDesdeWeb(url: URL(string: (usuario.results![0].picture?.large)!)!)
        
        // Nombre
        etiquetaTítulo.text = (usuario.results![0].name?.title)
        etiquetaNombre.text = usuario.results![0].name?.first
        etiquetaApellido.text = usuario.results![0].name?.last
        if etiquetaTítulo.text!.count <= 3 {
            etiquetaTítulo.text?.append(".")
        }
        
        // Género
        if usuario.results![0].gender == "male" {
            imagenGénero.image = UIImage(named: "Hombre")
        } else {
            imagenGénero.image = UIImage(named: "Mujer")
        }
    }
    
    private func realizarPeticiónDePerfil() {
        view.addSubview(animaciónCarga)
        mandarPeticiónGET { (respuesta) in
            DispatchQueue.main.async {
                self.animaciónCarga.removeFromSuperview()
            }
            print ("Respuesta del sevidor:")
            print (String(data: respuesta, encoding: .utf8))
            let datosUsuario = self.validarRespuestaPetición(para: respuesta)
            if  datosUsuario != nil {
                print("Tenemos una estructura JSON válida")
                DispatchQueue.main.async {
                    self.vaciarDatosEnPantalla(para: datosUsuario!)
                }
            } else {
                print("No tenemos una estructura válida")
            }
        }
    }
}
