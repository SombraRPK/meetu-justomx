//
//  Funciones.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 04/12/20.
//

import Foundation
import UIKit

func mandarPeticiónGET(envío: @escaping (Data) -> Void) {
    let dirección = "https://randomuser.me/api"
    let petición = URLSession.shared.dataTask(with: URL(string: dirección)!) { (datos, respuesta, error) in
        if (error == nil) {
            print ("Petición exitosa")
            envío(datos!)
        } else {
            print ("Error al obtener datos")
        }
    }
    petición.resume()
}

func darFormatoAFecha(fecha: String) -> String {
    var fechaFinal = String()
    print ("Fecha que llega del servidor: \(fecha)")
    let formateadorFechaServidor = DateFormatter()
    formateadorFechaServidor.locale = Locale (identifier: "es_MX")
    formateadorFechaServidor.dateFormat = "yyyy-MM-dd'T'HH:mm:ss.SSSZ"
    let fechaParaDescomponer = formateadorFechaServidor.date (from: fecha)!
    let calendario = Calendar.current
    let componentes = calendario.dateComponents([.year, .month, .day], from: fechaParaDescomponer)
    let fechaDescompuesta = calendario.date (from: componentes)
    let formateadorFechaFinal = DateFormatter()
    formateadorFechaFinal.dateFormat = "dd/MM/yyyy"
    fechaFinal = formateadorFechaFinal.string(from: fechaDescompuesta!)
    print ("Fecha final: \(fechaFinal)")
    return fechaFinal
}

