//
//  Estructuras.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 04/12/20.
//

import Foundation

struct datosPersonalesUsuario: Codable {
    var results: [results]?
    var info: info?
}

struct results: Codable {
    var gender: String?
    var name: name?
    var location: location?
    var email: String?
    var login: login?
    var dob: dob?
    var registered: registered?
    var phone: String?
    var cell: String?
    var id: id?
    var picture: picture?
    var nat: String?
}

struct name: Codable {
    let title: String?
    let first: String?
    let last: String?
}

struct location: Codable {
    let street: street?
    let city: String?
    let state: String?
    let country: String?
    let postcode: cadenaOEntero?
    let coordinates: coordinates?
    let timezone: timezone?
}

struct street: Codable {
    let number: Int?
    let name: String?
}

struct coordinates: Codable {
    let latitude: String?
    let longitude: String?
}

struct timezone: Codable {
    let offset: String?
    let description: String?
}

struct login: Codable {
    let uuid: String?
    let username: String?
    let password: String?
    let salt: String?
    let md5: String?
    let sha1: String?
    let sha256: String?
}

struct dob: Codable {
    let date: String?
    let age: Int?
}

struct registered: Codable {
    let date: String?
    let age: Int?
}

struct id: Codable {
    let name: String?
    let value: String?
}

struct picture: Codable {
    let large: String?
    let medium: String?
    let thumbnail: String?
}

struct info: Codable {
    let seed: String?
    let results: Int?
    let page: Int?
    let version: String?
}

// Utilizo esta estructura para localizar en MapKit
struct coordenadasFlotantes {
    let latitud: Float
    let longitud: Float
}

enum cadenaOEntero: Codable {
    case cadena (String)
    case entero (Int)
    
    init(from decoder: Decoder) throws {
        if let entero = try? decoder.singleValueContainer().decode(Int.self){
            self = .entero(entero)
            return
        }
        
        if let cadena = try? decoder.singleValueContainer().decode(String.self){
            self = .cadena(cadena)
            return
        }
        
        enum Error: Swift.Error {
            case niEnteroNiCadena
        }
        
        throw Error.niEnteroNiCadena
    }
    
    func encode(to encoder: Encoder) throws {
    }
}
