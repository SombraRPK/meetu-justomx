//
//  Clases.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 05/12/20.
//

import Foundation
import UIKit

// Botones redondos

class botónRedondo: UIButton {
    required init?(coder codificador: NSCoder) {
        super.init(coder: codificador)
        self.layer.cornerRadius = self.frame.height/2
        self.layer.shadowRadius = 10
        self.layer.shadowColor = UIColor.darkGray.cgColor
        self.layer.shadowOffset = CGSize (width: 0, height: 0)
    }
}

extension UIImageView {
    func cargarDesdeWeb(url: URL) {
        DispatchQueue.global().async { [weak self] in
            if let data = try? Data(contentsOf: url) {
                if let image = UIImage(data: data) {
                    DispatchQueue.main.async {
                        self?.image = image
                    }
                }
            }
        }
    }
}
