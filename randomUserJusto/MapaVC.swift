//
//  MapaVC.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 07/12/20.
//

import UIKit
import MapKit
import CoreLocation

class MapaVC: UIViewController, MKMapViewDelegate {
    
    var coordenadasRecibidas = coordenadasFlotantes(latitud: 0.0, longitud: 0.0)
    var nombre = ""
    
    @IBOutlet weak var vistaMapa: MKMapView!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        localizarEnMapa()
    }
    
    func localizarEnMapa(){
        // Asignamos el delegate
        vistaMapa.delegate = self
        
        // Preparamos las coordenadas
        let latitud = CLLocationDegrees(coordenadasRecibidas.latitud)
        let longitud = CLLocationDegrees(coordenadasRecibidas.longitud)
        let coordenadas: CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitud, longitud)
        
        // Preparamos la anotación para colocar en el mapa
        let anotación = MKPointAnnotation()
        anotación.title = nombre
        anotación.coordinate = CLLocationCoordinate2DMake(CLLocationDegrees(coordenadas.latitude), CLLocationDegrees(coordenadas.longitude))
        vistaMapa.addAnnotation(anotación)
        
        // Hacemos zoom para ver el lugar específico
        let punto = MKCoordinateSpan (latitudeDelta: 0.1, longitudeDelta: 0.1)
        let región = MKCoordinateRegion (center: coordenadas, span: punto)
        vistaMapa.setRegion(región, animated: true)
    }
}
