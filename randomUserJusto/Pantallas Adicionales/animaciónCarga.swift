//
//  animaciónCarga.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 07/12/20.
//

import Foundation
import UIKit
import Lottie

class AnimaciónCarga: UIView {
    
    let vistaAnimación = AnimationView()
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("animaciónCarga", owner: self, options: nil)
        
        print ("Mostrando animación carga")
        prepararAnimación()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    func prepararAnimación() {
        self.backgroundColor = UIColor(red: 0, green: 0, blue: 0, alpha: 0.75)
        self.frame = UIScreen.main.bounds
        
        // Animación
        vistaAnimación.frame.size.height = 200
        vistaAnimación.frame.size.width = 200
        vistaAnimación.center = self.center
        vistaAnimación.animation = Animation.named ("AnimaciónCarga")
        
        vistaAnimación.contentMode = .scaleAspectFit
        vistaAnimación.loopMode = .loop
        vistaAnimación.play()
        self.addSubview(vistaAnimación)
    }
}
