//
//  alertaInteresante.swift
//  randomUserJusto
//
//  Created by Reivaj Gómez Pérez on 07/12/20.
//

import Foundation
import UIKit

class AlertaInteresante: UIView {

    static let instancia = AlertaInteresante()
    
    @IBOutlet weak var vistaContenedor: UIView!
    @IBOutlet weak var vistaAlerta: UIView!
    @IBOutlet weak var botónFinalizar: UIButton!
    @IBOutlet weak var imagenEstrella: UIImageView!
    
    @IBAction func botónFinalizar(_ sender: Any) {
        vistaContenedor.removeFromSuperview()
    }
    
    override init(frame: CGRect) {
        super.init(frame: frame)
        Bundle.main.loadNibNamed("alertaInteresante", owner: self, options: nil)
        prepararUI()
    }
    
    required init?(coder aDecoder: NSCoder) {
       super.init(coder: aDecoder)
    }
    
    func prepararUI(){
        
        // Vista contenedor
        vistaContenedor.frame.size.width = UIScreen.main.bounds.width
        vistaContenedor.frame.size.height = UIScreen.main.bounds.height
        
        // Imagen estrella
        imagenEstrella.frame.size = CGSize(width: 100, height: 100)
        imagenEstrella.center.x = vistaContenedor.center.x
        imagenEstrella.center.y = vistaContenedor.center.y - 120
        
        // Vista alerta
        vistaAlerta.layer.cornerRadius = 15
        vistaAlerta.center.x = vistaContenedor.center.x
        vistaAlerta.center.y = vistaContenedor.center.y
        
        // Botón
        botónFinalizar.layer.cornerRadius = 10
        botónFinalizar.tintColor = .darkGray
        
    }
    
    func mostrarAlerta() {
        UIApplication.shared.keyWindow?.addSubview(vistaContenedor)
    }
}
